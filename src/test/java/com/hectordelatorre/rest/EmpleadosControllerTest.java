package com.hectordelatorre.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena() {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));

    }


    @Test
    public void testGetAutor(){
        assertEquals("HectordelaTorre", empleadosController.getAppAutor());
    }

}
