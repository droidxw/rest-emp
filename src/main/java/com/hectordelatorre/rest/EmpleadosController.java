package com.hectordelatorre.rest;

import com.hectordelatorre.rest.empleados.Capacitacion;
import com.hectordelatorre.rest.empleados.Empleado;
import com.hectordelatorre.rest.empleados.Empleados;
import com.hectordelatorre.rest.repositorios.EmpleadoDAO;
import com.hectordelatorre.rest.utils.Configuracion;
import com.hectordelatorre.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/empleados")
public class EmpleadosController{

@Autowired
    private EmpleadoDAO empDAO;

@GetMapping(path="/")
    public Empleados getEmpleados(){
    return empDAO.getAllEmpleados();

}

   @GetMapping(path="/{id}")
    public ResponseEntity<Empleado> getEmpleados(@PathVariable int id){
    Empleado emp = empDAO.getEmpleado(id);
        if(emp==null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().body(emp);
        }
             //
    }
@PostMapping ("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
    Integer id=empDAO.getAllEmpleados().getListaEmpleados().size()+1;
    emp.setId(id);
    empDAO.addEmpleado(emp) ;
    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(emp.getId())
            .toUri();
    return ResponseEntity.created(location).build();
}
@PutMapping(path="/", consumes="application/json", produces="application/json")
    public ResponseEntity<Object>updEmpleado(@RequestBody Empleado emp){
    empDAO.updEmpleado(emp);
    return ResponseEntity.ok().build();
}


    @PutMapping(path="/{id}", consumes="application/json", produces="application/json")
    public ResponseEntity<Object>updEmpleado(@PathVariable int id,@RequestBody Empleado emp){
        empDAO.updEmpleado(id,emp);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping (path="/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id){
    String resp= empDAO.deleteEmpleado(id);
    if(resp=="OK"){
        return ResponseEntity.ok().build();

    }else{
        return ResponseEntity.notFound().build();
        }
    }


    @PatchMapping( path="/{id}", consumes="application/json", produces="application/json")
    public ResponseEntity<Object>softupdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates )
    {
        empDAO.softupdEmpleado(id,updates);
        return ResponseEntity.ok().build();

    }

    @GetMapping(path="/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>>getCapacitacionesEmpleado(@PathVariable int id){
        return ResponseEntity.ok().body(empDAO.getCapacitacionesEmpleado(id));
    }


    @PostMapping ( path="/{id}/capacitaciones", consumes="application/json", produces="application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id,@RequestBody Capacitacion cap) {
if(empDAO.addCapacitacion(id,cap)) {
    return ResponseEntity.ok().build();
    }else{return ResponseEntity.notFound().build();
        }
    }
    @Value("${app.titulo}") private String titulo;

    @GetMapping(path="/titulo")
    public String getAppTitulo(){
String modo = configuracion.getModo();
       // return titulo;
        return String.format("%s (%s)", titulo, modo);
    }
    @Autowired
    private Environment env;
//Devuelve el autor

//Gestion de propiedades
@Autowired
    Configuracion configuracion;


    @GetMapping(path="/autor")
    public String getAppAutor(){
        return configuracion.getAutor();
        //return env.getProperty("app.autor");
    }

    @GetMapping(path="/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador){

        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception ex) {return "";}


    }



}