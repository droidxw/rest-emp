package com.hectordelatorre.rest.empleados;

import java.util.ArrayList;
import java.util.List;

public class Empleados {
    private List<Empleado> listaEmpleados;

    public List<Empleado> getListaEmpleados() {
        if (listaEmpleados == null) {
            listaEmpleados = new ArrayList<>();
        }
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
}
