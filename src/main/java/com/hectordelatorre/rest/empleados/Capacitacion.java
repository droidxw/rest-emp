package com.hectordelatorre.rest.empleados;

public class Capacitacion {
    private String fecha;
    private String titulo;

    public Capacitacion() {
    }

    public Capacitacion(String fecha, String titulo) {
        this.fecha = fecha;
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return "Capacitacion{" +
                "fecha='" + fecha + '\'' +
                ", titulo='" + titulo + '\'' +
                '}';
    }
}
